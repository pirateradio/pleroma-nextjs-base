import { useState, useCallback, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import generator, { OAuth } from 'megalodon';

const scopes = ['read', 'write', 'follow']; // https://docs.joinmastodon.org/api/oauth-scopes/
const baseUrl = 'https://pirateradio.social';
const appClient = generator('pleroma', baseUrl);

const AuthPage = () => {
  const [loading, setLoading] = useState(true);
  const [appData, setAppData] = useState(null);
  const [authToken, setAuthToken] = useState('');
  const [_, setCookies] = useCookies(['user']);
  const router = useRouter();

  useEffect(() => {
    (async () => {
      const appData = await appClient.registerApp('Test App', { scopes });
      setAppData(appData);
      setLoading(false);
    })();
  }, []);

  const handleLogin = useCallback(async () => {
    setLoading(true);
    const tokenData: OAuth.TokenData = await appClient.fetchAccessToken(appData.clientId, appData.clientSecret, authToken);
    const userClient = generator('pleroma', baseUrl, tokenData.accessToken);
    const { data: { acct } } = await userClient.verifyAccountCredentials();
    setCookies('user', { username: acct }, {
      path: '/',
      maxAge: 3600, // 1 hour,
      sameSite: true,
    });

    router.push('/');
  }, [authToken]);

  if (loading) return <h1>Loading...</h1>;

  return (
    <>
      <h1>Auth Required</h1>
      <a href={appData.url} target="_blank">Click here to get your access token.</a>
      <br />
      <input
        type="text"
        value={authToken}
        onChange={e => setAuthToken(e.target.value.trim())}
        placeholder="Auth Token"
      />
      <button onClick={handleLogin} disabled={!authToken}>Login</button>
    </>
  );
}

export default AuthPage;
