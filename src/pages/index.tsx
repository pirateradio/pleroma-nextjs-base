import type { GetServerSideProps } from 'next';
import { useCookies } from 'react-cookie';
import { authRedirect } from '@/helpers/cookies';

const IndexPage = () => {
  const [{ user }] = useCookies(['user']);

  return <h1>Hello, @{user.username}!</h1>
};

export const getServerSideProps: GetServerSideProps = async (context) => authRedirect(context.req);

export default IndexPage;
