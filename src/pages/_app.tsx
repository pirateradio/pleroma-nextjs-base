import App from 'next/app';
import type { AppProps, AppContext } from 'next/app';
import { CookiesProvider, Cookies } from "react-cookie"

const isBrowser = () => typeof window !== "undefined";

const MyApp = ({ Component, pageProps: { cookies, ...rest } }: AppProps) => (
  <CookiesProvider cookies={isBrowser() ? undefined : cookies}>
    <Component {...rest} />
  </CookiesProvider>
);

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);
  const cookies = new Cookies(appContext.ctx.req.headers.cookie);
  appProps.pageProps.cookies = cookies;

  return { ...appProps };
};

export default MyApp;
