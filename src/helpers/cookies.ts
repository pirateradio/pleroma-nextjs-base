import { Cookies } from 'react-cookie';

export const authRedirect = (req?) => {
  const cookies = new Cookies(req.headers.cookie || '');
  if (!cookies.get('user')) {
    return {
      redirect: {
        destination: '/auth',
        permanent: false,
      },
    };
  }
  return { props: {} };
};
