module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.node = {
      ...config.node,
      net: 'empty',
      tls: 'empty',
      dns: 'empty',
    };
    return config;
  },
}
